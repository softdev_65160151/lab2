/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.ox;

import java.util.Scanner;

/**
 *
 * @author Pare
 */
public class OX {

    static void printWelcome() {
        System.out.println("Welcome To XO Game!");
    }
    static char[][] table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}};

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println(" ");
        }
    }
    
    static void newTable(){
        for (int i = 0;i<3;i++){
            for(int j = 0;j<3;j++){
                table[i][j] = '-';
            }
        }
        printTable();
    }
    
    
    static char Player = 'X';

    static void Turn() {
        System.out.println(Player + " Turn");
    }

    static int row, col;

    static void inputRC() {
        Scanner kb = new Scanner(System.in);
        while (true) { 
            System.out.print("Please input row col: ");
            row = kb.nextInt();
            col = kb.nextInt();
            if(table[row-1][col-1] != '-' ) { //check duplicate
                continue;
            }
            table[row - 1][col - 1] = Player;
            break;
        }

    }
    
    static boolean isWin(){
        //check row
        for(int i = 0; i<3; i++){
            if (table[i][0] == Player && table[i][1] == Player && table[i][2] == Player){
                return true;
            }
        }
        
        //check col
        for (int j =0;j<3; j++){
            if(table[0][j]== Player && table[1][j] == Player && table [2][j] == Player){
                return true;
            }
        }
        
         //check diagonals
        if ((table[0][0]== Player && table[1][1] == Player && table[2][2] == Player )||
          (table[0][2] == Player && table[1][1] == Player && table[2][0] == Player)){
            return true;
        }
        
        return false;  
    }  
    
    static void printWin(){
        System.out.print(Player+" Win!!\n");
    }
    
    static boolean isDraw(){
        for(int i =0; i<3;i++){
            for(int j=0; j<3;j++){
                if(table[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    }
    
    static void printDraw(){
        System.out.print("Draw!\n");
    }
     
    static void switchP(){
        if(Player == 'X'){
            Player = 'O';
        }else{
            Player = 'X';
        }
    }
    
    static void printGameOver(){
        System.out.print("Game Over !!\n");
    }
    
    static void isCon() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Continue [y/n]: ");
        String con = kb.next();
        if (con.equals("y")) {
            printWelcome();
            newTable();
            Turn();
            inputRC();
        } else if (con.equals("n")) {
            System.exit(0);
        }
    }
    
    public static void main(String[] args){
        printWelcome();
        while (true) {
            printTable();
            Turn();
            inputRC();
            if(isWin()){
                printTable();
                printWin();
                printGameOver();
                isCon();
            }else if(isDraw()){
                printTable();
                printDraw();
                printGameOver();
                isCon();           
            }
            switchP();
        }

    }
}
